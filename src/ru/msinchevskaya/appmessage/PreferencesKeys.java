package ru.msinchevskaya.appmessage;

public final class PreferencesKeys {
	
	private PreferencesKeys(){};
	
	
	//I
	public static final String id = "id";
	
	//P
	public static final String PREF_NAME = "AppMessagePrefs";
}
