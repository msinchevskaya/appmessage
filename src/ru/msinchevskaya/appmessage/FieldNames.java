package ru.msinchevskaya.appmessage;

public final class FieldNames {
	
	private FieldNames(){}
	
	//I
	public static final String id = "_id";
	
	//T
	public static final String text = "text";

}
