package ru.msinchevskaya.appmessage.objects;

import ru.msinchevskaya.appmessage.FieldNames;
import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

public final class UserMessage implements Parcelable{
	
	private final int id;
	private final String text;
	
	public UserMessage(int id, String text){
		this.id = id;
		this.text = text;
	}
	
	private UserMessage(Parcel parcel){
		id = parcel.readInt();
		text = parcel.readString();
	}
	
	public int getId(){
		return id;
	}
	
	public String getText(){
		return text;
	}
	
	public ContentValues toContentValues(){
		final ContentValues cv = new ContentValues();
		cv.put(FieldNames.id, id);
		cv.put(FieldNames.text, text);
		return cv;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
	    dest.writeInt(id);
	    dest.writeString(text);
	}
	
	  public static final Parcelable.Creator<UserMessage> CREATOR = new Parcelable.Creator<UserMessage>() {
		    public UserMessage createFromParcel(Parcel in) {
		      return new UserMessage(in);
		    }

		    public UserMessage[] newArray(int size) {
		      return new UserMessage[size];
		    }
		  };
}
