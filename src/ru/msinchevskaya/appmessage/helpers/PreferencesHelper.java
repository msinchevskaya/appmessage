package ru.msinchevskaya.appmessage.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesHelper {
	
	public static final String TAG = "ru.msinchevskaya.apmessage.helpers.PreferenceHelper";
	private static volatile PreferencesHelper sInstance;
	private SharedPreferences mPrefs;
	
	private PreferencesHelper(){};
	
	public static PreferencesHelper getInstance() {
		PreferencesHelper instance = sInstance;
		if (instance == null) {
			synchronized (PreferencesHelper.class) {
				instance = sInstance;
				if (instance == null) {
					instance = sInstance = new PreferencesHelper();
				}
			}
		}
		return sInstance;
	}
	
	public void initialize(Context context){
		mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public int getInt(String key){
		return mPrefs.getInt(key, 0);
	}
	
	public void putInt(String key, int value){
		mPrefs.edit().putInt(key, value).commit();
	}

}
