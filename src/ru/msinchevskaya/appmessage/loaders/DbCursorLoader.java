package ru.msinchevskaya.appmessage.loaders;

import ru.msinchevskaya.appmessage.database.DataBaseAccessor;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;

public class DbCursorLoader implements LoaderCallbacks<Cursor>{

	private SimpleCursorAdapter mAdapter;
	private Context mContext;
	
	
	public DbCursorLoader(Context context, SimpleCursorAdapter adapter){
		mContext = context;
		mAdapter =  adapter;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		return new DbLoader(mContext);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		mAdapter.swapCursor(cursor);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}
	
	private static class DbLoader extends CursorLoader {

		public DbLoader(Context context) {
			super(context);
		}
		
		@Override
	    public Cursor loadInBackground() {
	      Cursor cursor = DataBaseAccessor.getInstance().query();
	      return cursor;
	    }
		
	}
	

}
