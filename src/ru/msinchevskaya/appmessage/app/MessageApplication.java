package ru.msinchevskaya.appmessage.app;

import ru.msinchevskaya.appmessage.database.DataBaseAccessor;
import ru.msinchevskaya.appmessage.helpers.PreferencesHelper;
import android.app.Application;

public class MessageApplication extends Application {
	
	public static final String TAG = "ru.msinchevskaya.appmessage.app.MessageApplication";
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		DataBaseAccessor.getInstance().initialize(getApplicationContext());
		PreferencesHelper.getInstance().initialize(getApplicationContext());
	}

}
