package ru.msinchevskaya.appmessage.app;

import java.lang.ref.WeakReference;
import java.util.Locale;

import ru.msinchevskaya.appmessage.R;
import ru.msinchevskaya.appmessage.database.DataBaseProvider;
import ru.msinchevskaya.appmessage.objects.UserMessage;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

public class NotificationService extends Service{
	

	public static final String TAG = "ru.msinchevskaya.appmessage.app.NotificationService";
	
	public final static String NOTIFICATION_ACTION = "action_notification";
	
	private final static int DELAY_MILLINS = 20 * 1000;
	private Handler mHandler;
	private DataBaseCheck mCheck;
	private DataBaseProvider mProvider;
	private NotificationManager mNotificationManager;
	
	@Override
	public void onCreate() {		
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		
		mHandler = new NotificationHandler(this);
		
		mProvider =new DataBaseProvider();
		mProvider.init();
		
		mCheck = new DataBaseCheck();
		mHandler.postDelayed(mCheck, DELAY_MILLINS);
		super.onCreate();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onDestroy() {		
		mHandler.removeCallbacks(mCheck);
		super.onDestroy();
	}
	
	private Intent createIntent(){
		Intent intent = new Intent();
		intent.setAction(NOTIFICATION_ACTION);
		return intent;
	}
	
	private void sendNotif(){
		final int count = mProvider.getNewMessages().size();
		final String titleString = String.format(Locale.getDefault(),"%d %s", count, getString(R.string.notif_title));
		StringBuilder sBuilder = new StringBuilder();
		
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
		.setSmallIcon(android.R.drawable.ic_dialog_email)
		.setContentTitle(titleString)
		.setDefaults(Notification.DEFAULT_VIBRATE);
		
		for (UserMessage mes : mProvider.getNewMessages()){
			sBuilder.append(mes.getText());
			sBuilder.append("\n");
		}
		
		mBuilder.setContentText(sBuilder.toString());
		Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
		stackBuilder.addParentStack(SecondActivity.class);
		stackBuilder.addNextIntent(intent);
		
		PendingIntent pIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		mBuilder.setContentIntent(pIntent);
		
		Notification notif = mBuilder.build();
		notif.flags = Notification.FLAG_AUTO_CANCEL;
		notif.number=count;
		
		mNotificationManager.notify(0, notif);

		mProvider.restart();
	}
	
	private class DataBaseCheck implements Runnable{

		public static final String TAG = "ru.msinchevskaya.appmessage.app.SecondActivity.DataBaseCheck";
		@Override
		public void run() {
			
			if (mProvider.isBaseUpdate()){
				Log.i(TAG, "DB updates");
				mHandler.sendEmptyMessage(1);
				sendBroadcast(createIntent());
			}
			mHandler.postDelayed(this, DELAY_MILLINS);
		}
	}
	
	private static class NotificationHandler extends Handler {
		
        WeakReference<NotificationService> wrService;
        
		public NotificationHandler(NotificationService service){
			wrService = new WeakReference<NotificationService>(service);
		}
		
		@Override
        public void handleMessage(Message msg) {
                super.handleMessage(msg);
                NotificationService service = wrService.get();
                if (service != null)
                	service.sendNotif();
        }
	}
}
