package ru.msinchevskaya.appmessage.app;

import ru.msinchevskaya.appmessage.R;
import ru.msinchevskaya.appmessage.database.DataBaseAccessor;
import ru.msinchevskaya.appmessage.objects.UserMessage;
import android.support.v4.app.FragmentActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class FirstActivity extends FragmentActivity {

	public static final String TAG = "ru.msinchevskaya.appmessage.app.FirstActivity";
	public static final String MESSAGE_ACTION = "ru.msinchevskaya.appmessage.app.messagefilter";
	public static final String EXTRA_MESSAGE = "extraMessage";
	private EditText etMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		etMessage = (EditText) findViewById(R.id.et_message);
	}

	public void onSaveClick(View v){
		String s = etMessage.getText().toString();
		if (s.length() > 0){
			startService(createIntent(etMessage.getText().toString()));
			etMessage.setText("");
			Toast.makeText(getApplicationContext(), getString(R.string.send_to_db), Toast.LENGTH_LONG).show();
		}
		else{
			Toast.makeText(getApplicationContext(), getString(R.string.empty_message), Toast.LENGTH_LONG).show();
		}
	}

	private Intent createIntent(String text){
		UserMessage mes = new UserMessage(DataBaseAccessor.getInstance().getNextId(), text);
		Intent intent = new Intent(getApplicationContext(), SaveService.class);
		intent.putExtra(EXTRA_MESSAGE, mes);
		return intent;
	}

	public void onNextClick(View v){
		startActivity(new Intent(getApplicationContext(), SecondActivity.class));
		Log.i(TAG, "sendMessage");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		stopService(new Intent(this, SaveService.class));
	}
}
