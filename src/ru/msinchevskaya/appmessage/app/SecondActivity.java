package ru.msinchevskaya.appmessage.app;

import ru.msinchevskaya.appmessage.FieldNames;
import ru.msinchevskaya.appmessage.R;
import ru.msinchevskaya.appmessage.loaders.DbCursorLoader;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SimpleCursorAdapter;
import android.widget.ListView;

public class SecondActivity extends FragmentActivity {
	
	public static final String TAG = "ru.msinchevskaya.appmessage.app.SecondActivity";

	private SimpleCursorAdapter mAdapter;
	private ListView mListView;
	private DbCursorLoader mCursorLoader;
	private NotificationReciever mReciever;
	
	
	@Override
	protected void onResume(){
		super.onResume();
		startService(new Intent(getApplicationContext(), NotificationService.class));
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		
		final String from[] = {FieldNames.id, FieldNames.text};
		final int[] to = {R.id.tv_id, R.id.tv_message};
				
		mAdapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.row_message, null, from, to, 0);
		mListView = (ListView) findViewById(R.id.lv_message);
		mListView.setAdapter(mAdapter);
		
		mCursorLoader = new DbCursorLoader(getApplicationContext(), mAdapter);
		
		mReciever = new NotificationReciever();
		
		IntentFilter filter = new IntentFilter(NotificationService.NOTIFICATION_ACTION);
		registerReceiver(mReciever, filter);
		
		getSupportLoaderManager().initLoader(0, null, mCursorLoader);
	}
	
	@Override	
	protected void onDestroy(){
		super.onDestroy();
		unregisterReceiver(mReciever);
	}
	
	private class NotificationReciever extends BroadcastReceiver{

		@Override
		public void onReceive(Context context, Intent intent) {
			getSupportLoaderManager().getLoader(0).forceLoad();
		}
		
	}
}
