package ru.msinchevskaya.appmessage.app;
import ru.msinchevskaya.appmessage.TableNames;
import ru.msinchevskaya.appmessage.database.DataBaseAccessor;
import ru.msinchevskaya.appmessage.objects.UserMessage;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;


public class SaveService extends Service {
	
	public static final String TAG = "ru.msinchevskaya.appmessage.app.SaveService";
	private SaveHandler mHandler;
	private static final int DELAY_MILLIS = 1000 * 60;
	
	public void onCreate(){
		mHandler = new SaveHandler();
	}
	
	public int onStartCommand(Intent intent, int flags, int startId){
		Message message = mHandler.obtainMessage();
		message.obj = intent.getParcelableExtra(FirstActivity.EXTRA_MESSAGE);
		mHandler.sendMessageDelayed(message, DELAY_MILLIS);
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	public void onDestroy(){
		super.onDestroy();
	}
	
	private static class SaveHandler extends Handler {
		
		public static final String TAG = "ru.msinchevskaya.appmessage.app.SaveService.SaveHandler";
		
		public void handleMessage(Message msg) {
			UserMessage mes = (UserMessage) msg.obj;
			DataBaseAccessor.getInstance().insert(TableNames.message, mes.toContentValues());
			Log.i(TAG, mes.getText());
	      };
	}

}
