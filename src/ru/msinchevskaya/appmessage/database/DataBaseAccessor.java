package ru.msinchevskaya.appmessage.database;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import ru.msinchevskaya.appmessage.PreferencesKeys;
import ru.msinchevskaya.appmessage.TableNames;
import ru.msinchevskaya.appmessage.helpers.PreferencesHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseAccessor {

	public static final String TAG = "ru.msinchevskaya.appmessage.database.DataBaseAccessor";
	private static final String DATABASE_NAME = "metadata.sqlite";
	private static final int DATABASE_VERSION = 1;
	private int id;

	private static volatile DataBaseAccessor sInstance;
	
	private DataBaseHelper mDataBaseHelper;
	private final Set<DataBaseChangeListener> mChangeLsiteners = new CopyOnWriteArraySet<DataBaseChangeListener>();
	
	public interface DataBaseChangeListener{
		public void notifyChanged();
	}
	
	private DataBaseAccessor(){};
	
	public static DataBaseAccessor getInstance() {
		DataBaseAccessor instance = sInstance;
		if (instance == null) {
			synchronized (DataBaseAccessor.class) {
				instance = sInstance;
				if (instance == null) {
					instance = sInstance = new DataBaseAccessor();
				}
			}
		}
		return sInstance;
	}
	
	public void initialize(Context context) {
		mDataBaseHelper = new DataBaseHelper(context, DATABASE_NAME, DATABASE_VERSION);
		}
	
	public Cursor query() {
		return mDataBaseHelper.getReadableDatabase().query(TableNames.message, null, null, null, null, null, null);
	}
	
	public void setDataBaseChangeListener(DataBaseChangeListener listener){
		mChangeLsiteners.add(listener);
	}
	
	public int getNextId(){
		id = PreferencesHelper.getInstance().getInt(PreferencesKeys.id);
		id++;
		PreferencesHelper.getInstance().putInt(PreferencesKeys.id, id);
		return id;
	}

	public void execute(String sql) {
		Log.d(TAG, "Sql execute: " + sql);
		mDataBaseHelper.getWritableDatabase().execSQL(sql);
	}

	public void insert(String table, ContentValues values) {
		mDataBaseHelper.getWritableDatabase().insert(table, null, values);
		
		for (DataBaseChangeListener listener : mChangeLsiteners){
			listener.notifyChanged();
		}
	}

	public void update(String table, long id, ContentValues values) {
		mDataBaseHelper.getWritableDatabase().update(table, values, "id=" + id, null);
	}
	
	private class DataBaseHelper extends SQLiteOpenHelper{

//		public static final String TAG = "ru.msinchevskaya.appmessage.database.DataBaseAccessor.DataBaseHelper";
		
		private static final String SQL_TABLE_MESSAGE =
				"CREATE TABLE message("
						+ "_id integer primary key, "
						+ "text text"
						+ ")";

		public DataBaseHelper(Context context, String name, int version) {
			super(context, name, null, version);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			onUpgrade(db, 0, DATABASE_VERSION);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			db.beginTransaction();
			try {
				switch (oldVersion) {
				case 0:
					db.execSQL(SQL_TABLE_MESSAGE);

				case DATABASE_VERSION:
					db.setTransactionSuccessful();

				default:
					break;
				}
			} finally {
				db.endTransaction();
			}
		}

	}
}
