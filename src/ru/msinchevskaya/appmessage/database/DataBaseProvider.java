package ru.msinchevskaya.appmessage.database;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;

import ru.msinchevskaya.appmessage.FieldNames;
import ru.msinchevskaya.appmessage.database.DataBaseAccessor.DataBaseChangeListener;
import ru.msinchevskaya.appmessage.objects.UserMessage;

public class DataBaseProvider implements DataBaseChangeListener{

	public boolean isUpdate;
	private int rowCount;
	
	public void init(){
		rowCount = DataBaseAccessor.getInstance().query().getCount();
		DataBaseAccessor.getInstance().setDataBaseChangeListener(this);
		isUpdate = false;
	}
	
	public boolean isBaseUpdate(){
		return isUpdate;
	}
	
	public List<UserMessage> getNewMessages(){
		
		final List<UserMessage> list = new ArrayList<UserMessage>();

		Cursor cursor = DataBaseAccessor.getInstance().query();
		cursor.moveToPosition(rowCount - 1);
		while (cursor.moveToNext()){ 
			UserMessage msg;
			final int id = cursor.getInt(cursor.getColumnIndex(FieldNames.id));
			final String text = cursor.getString(cursor.getColumnIndex(FieldNames.text));
			msg = new UserMessage(id, text);
			list.add(msg);
		}
		return list;
	}
	
	public void restart(){
		rowCount = DataBaseAccessor.getInstance().query().getCount();
		isUpdate = false;
	}

	@Override
	public void notifyChanged() {
		isUpdate = true;
	}

}
